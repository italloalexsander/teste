﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostController : PlayerController {

	// Use this for initialization
	override protected void Start () {
        base.Start();
        isGhostCharacter = true;
    }

    protected override void Interact()
    {
        if (collidedObject)
        {
            collidedObject.GetComponent<PlayerController>().Activate();

            if (collidedObject.transform.position == collidedObject.GetComponent<PlayerController>().checkpoint.transform.position)
                checkPointCharacter = collidedObject;

            Destroy(gameObject);
        }
    }

    override protected void OnTriggerEnterEvent(Collider2D obj)
    {
        collidedObject = obj.gameObject;
    }
}
