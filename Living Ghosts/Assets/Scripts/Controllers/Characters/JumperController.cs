﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumperController : PlayerController {

    public float wallCrawlSpeed = 15f;
    public float accelerationTimeWall = 0.05f;

    private bool canJumpAgain = false;
    private bool isOnWall = false;
    private bool isWallCrawling = false;
    private bool onGround = false;
    private int jumpCount = 0;
    private float velocityYSmoothing;
    private float velocityFactor = 1f;
    private RaycastHit2D collision;

    // Use this for initialization
    override protected void Start () {
        base.Start();
        characterController.onControllerCollidedEvent += onControllerCollidedEvent;
    }


    override protected void Update() {
        if (isActiveCharacter)
        {
            CanWallCrawl();

            velocity = characterController.velocity;

            if (characterController.isGrounded || isWallCrawling)
            {
                velocity.y = 0;
            }

            animator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));
            animator.SetBool("IsGrounded", characterController.isGrounded);

            if ((!isWallCrawling || (isWallCrawling && onGround)) && Input.GetAxis("Horizontal") > 0.001f)
            {
                GoRight();
            }
            else if ((!isWallCrawling || (isWallCrawling && onGround)) && Input.GetAxis("Horizontal") < -0.001f)
            {
                GoLeft();
            }
            else
            {
                velocity.x = 0;
            }

            if (isWallCrawling && Input.GetAxis("Vertical") > 0.001f)
            {
                GoUp();
            }
            else if (isWallCrawling && Input.GetAxis("Vertical") < -0.001f)
            {
                GoDown();
            }

            if (Input.GetButtonDown("Jump"))
            {
                Jump();
            }

            if (Input.GetKeyDown("q"))
                Interact();

        }
    }

    override protected void Move()
    {
        if (jumped)
        {
            jumped = false;
            animator.SetBool("Crawl", false);

            if (isWallCrawling)
            {
                animator.SetBool("Crawl", true);
                animator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Vertical")));
                print(Mathf.Abs(Input.GetAxis("Vertical")));

                if (collision.normal.x == 1f && Input.GetAxis("Horizontal") > 0.001f)
                {
                    velocity.y = jumpVelocity;
                    velocity.x = Mathf.SmoothDamp(velocity.x, moveSpeed * Input.GetAxis("Horizontal"), ref velocityXSmoothing, accelerationTimeWall);
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                }
                else if (collision.normal.x == -1f && Input.GetAxis("Horizontal") < -0.001f)
                {
                    velocity.y = jumpVelocity;
                    velocity.x = Mathf.SmoothDamp(velocity.x, moveSpeed * Input.GetAxis("Horizontal"), ref velocityXSmoothing, accelerationTimeWall);
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                }
                else
                    velocity.x = Mathf.SmoothDamp(velocity.x, moveSpeed * 1f, ref velocityXSmoothing, accelerationTimeWall);

                canJumpAgain = true;
                isWallCrawling = false;
                jumpCount++;
                animator.SetTrigger("Jump");
            }
            else if (characterController.isGrounded)
            {
                velocity.y = jumpVelocity;
                canJumpAgain = true;
                jumpCount++;
                animator.SetTrigger("Jump");
            }
            else if (canJumpAgain)
            {
                velocity.y = jumpVelocity;
                canJumpAgain = false;
                jumpCount++;
                animator.SetTrigger("Jump");
            }
            else if (jumpCount < 2)
            {
                velocity.y = jumpVelocity;
                canJumpAgain = true;
                animator.SetTrigger("Jump");
            }
        } else if (!jumped && characterController.isGrounded)
        {
            jumpCount = 0;
        }

        if (!isWallCrawling)
            velocity.y += gravity * Time.deltaTime;

        characterController.move(velocity * Time.deltaTime);
    }

    void GoUp()
    {
        velocity.y = Mathf.SmoothDamp(velocity.y, wallCrawlSpeed * Input.GetAxis("Vertical"), ref velocityYSmoothing, 0.01f);

        onGround = false;
    }

    void GoDown()
    {
        velocity.y = Mathf.SmoothDamp(velocity.y, wallCrawlSpeed * Input.GetAxis("Vertical"), ref velocityYSmoothing, 0.01f);

        if (characterController.isGrounded) onGround = true;
    }

    void CanWallCrawl()
    {
        if (!isOnWall)
            isWallCrawling = false;
        else if (collision.normal.y == -1f)
            isWallCrawling = false;
        else if (!isWallCrawling && collision.normal.x == 1f && Input.GetAxis("Horizontal") < -0.001f)
        {
            isWallCrawling = true;
            onGround = true;
        }
        else if (!isWallCrawling && collision.normal.x == -1f && Input.GetAxis("Horizontal") > 0.001f)
        {
            isWallCrawling = true;
            onGround = true;
        }
        else
            isWallCrawling = true;
    }

    override protected void Jump()
    {
        if (isActiveCharacter)
        {
            jumped = true;
        }
    }

    private void onControllerCollidedEvent(RaycastHit2D obj)
    {
        if (obj.normal.y == 1f)
            return;

        collision = obj;
    }

    override protected void onTriggerExitEvent(Collider2D obj)
    {
        base.onTriggerExitEvent(obj);

        if (obj.gameObject.layer == LayerMask.NameToLayer("Wall"))
            isOnWall = false;
    }

    override protected void OnTriggerEnterEvent(Collider2D obj)
    {
        base.OnTriggerEnterEvent(obj);

        if (obj.gameObject.layer == LayerMask.NameToLayer("Wall"))
            isOnWall = true;
    }
}
