﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public static GameObject checkPointCharacter;

    public float moveSpeed = 24f;
    public float jumpHeight = 8f;
    public float timeToJumpApex = 0.4f;
    public float accelerationTimeAir = 0.2f;
    public float accelerationTimeGround = 0.1f;
    public bool isActiveCharacter = false;
    public bool isGhostCharacter = false;
    public int hp = 3;
    private float scale = 1f;

    private int defaulthp;

    protected Vector3 startPosition;
    protected Vector3 startScale;
    protected Quaternion startRotation;

    public GameObject ghost;
    public GameObject character;
    public Transform checkpoint;

    protected float gravity;
    protected float jumpVelocity;
    protected float velocityXSmoothing;
    protected Vector2 velocity;
    protected bool jumped = false;

    protected Animator animator;
    protected CharacterController2D characterController;

    protected GameObject collidedObject = null;
    
    // Use this for initialization
    protected virtual void Start () {
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Laser"), false);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Playable"), false);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Platform"), false);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Bullet"), LayerMask.NameToLayer("Player"), false);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Bullet"), LayerMask.NameToLayer("Enemy"), false);

        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController2D>();

        characterController.onTriggerEnterEvent += OnTriggerEnterEvent;
        characterController.onTriggerExitEvent += onTriggerExitEvent;

        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;

        if (!isActiveCharacter)
        {
            GetComponent<BoxCollider2D>().isTrigger = true;
            GetComponent<SpriteRenderer>().sortingLayerName = "Playable";
        }

        defaulthp = hp;

        startPosition = transform.position;
        startScale = transform.localScale;
        startRotation = transform.rotation;

        scale = transform.localScale.x;
    }

    virtual protected void Update ()
    {
        if (isActiveCharacter)
        {
            velocity = characterController.velocity;

            if (characterController.isGrounded)
            {
                velocity.y = 0;
            }

            animator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));
            animator.SetBool("IsGrounded", characterController.isGrounded);

            if (Input.GetAxis("Horizontal") > 0.001f)
            {
                GoRight();
            }
            else if (Input.GetAxis("Horizontal") < -0.001f)
            {
                GoLeft();
            }
            else
            {
                velocity.x = 0;
            }

            if (Input.GetButtonDown("Jump"))
            {
                Jump();
            }

            if (Input.GetKeyDown("q"))
                Interact();

        }
    }

    virtual protected void FixedUpdate ()
    {
        if (isActiveCharacter)
        {
            Move();
        }
    }

    virtual protected void Move ()
    {
        velocity.y += gravity * Time.deltaTime;
        if (jumped)
        {
            jumped = false;
            velocity.y = jumpVelocity;
            animator.SetTrigger("Jump");
        }
        characterController.move(velocity * Time.deltaTime);
    }

    virtual protected void Jump ()
    {
        if (characterController.isGrounded)
        {
            jumped = true;
        }
     }

    protected void GoRight ()
    {
        velocity.x = Mathf.SmoothDamp(velocity.x, moveSpeed * Input.GetAxis("Horizontal"), ref velocityXSmoothing, characterController.isGrounded ? accelerationTimeGround : accelerationTimeAir);

        if (transform.localScale.x < 0f)
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    protected void GoLeft()
    {
        velocity.x = Mathf.SmoothDamp(velocity.x, moveSpeed * Input.GetAxis("Horizontal"), ref velocityXSmoothing, characterController.isGrounded ? accelerationTimeGround : accelerationTimeAir);

        if (transform.localScale.x > 0f)
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    virtual protected void Interact()
    {
        if (characterController.isGrounded)
        {
            GameObject ghostInstance = Instantiate(ghost, transform.position, transform.rotation);
            GhostController ghostController = ghostInstance.GetComponent<GhostController>();
            ghostController.Activate();
            ghostController.collidedObject = gameObject;
            Deactivate();
        }
    }

    virtual public void Activate()
    {
        var vcam = GameObject.FindGameObjectWithTag("VirtualCamera");
        var new_vcam = GameObject.Instantiate(vcam);
        new_vcam.GetComponent<Cinemachine.CinemachineVirtualCamera>().Follow = transform;
        vcam.SetActive(false);
        new_vcam.SetActive(true);
        //Destroy(vcam);

        isActiveCharacter = true;
        GetComponent<BoxCollider2D>().isTrigger = false;
        gameObject.layer = LayerMask.NameToLayer("Player");
        GetComponent<SpriteRenderer>().sortingLayerName = "Player";
    }

    virtual protected void Deactivate()
    {
        isActiveCharacter = false;
        GetComponent<BoxCollider2D>().isTrigger = true;
        gameObject.layer = LayerMask.NameToLayer("Playable");
        GetComponent<SpriteRenderer>().sortingLayerName = "Playable";
        if (animator) animator.SetFloat("Speed", 0f);
    }

    virtual public bool Hit(int damage)
    {
        hp -= damage;
        print(hp);

        if (hp <= 0)
        {
            Delete();
            return true;
        }
        return false;
    }

    public void ResetToCheckpoint()
    {
        GameObject characterInstance = Instantiate(character, checkpoint.position, checkpoint.rotation);
        characterInstance.transform.localScale = new Vector3(scale,  scale, scale);
        characterInstance.GetComponent<PlayerController>().Deactivate();
        characterInstance.GetComponent<PlayerController>().hp = defaulthp;
        checkPointCharacter = characterInstance;
        Destroy(gameObject);
    }

    protected void Delete()
    {
        GameObject ghostInstance = Instantiate(ghost, transform.position, transform.rotation);
        GhostController ghostController = ghostInstance.GetComponent<GhostController>();
        ghostController.Activate();

        ResetToCheckpoint();
    }

    public void ResetPosition()
    {
        gameObject.transform.position = startPosition;
        gameObject.transform.localScale = startScale;
        gameObject.transform.rotation = startRotation;
        hp = defaulthp;
    }

    virtual protected void onTriggerExitEvent(Collider2D obj)
    {
        if (collidedObject == obj.gameObject)
            collidedObject = null;
    }

    virtual protected void OnTriggerEnterEvent(Collider2D obj)
    {
        collidedObject = obj.gameObject;

        if (obj.gameObject.layer == LayerMask.NameToLayer("Laser"))
        {
            Delete();
        }
    }
}
