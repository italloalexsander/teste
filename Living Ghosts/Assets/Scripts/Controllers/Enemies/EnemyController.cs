﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    protected Vector3 startPosition;
    protected Vector3 startScale;
    protected Quaternion startRotation;
    public int hp = 3;
    private int starthp;

    protected virtual void Start()
    {
        startPosition = transform.position;
        startScale = transform.localScale;
        startRotation = transform.rotation;
        starthp = hp;
    }

    public void ResetPosition()
    {
        gameObject.transform.position = startPosition;
        gameObject.transform.localScale = startScale;
        gameObject.transform.rotation = startRotation;
        gameObject.SetActive(true);
        hp = starthp;
    }

    virtual public bool Hit(int damage)
    {
        hp -= damage;
        print(hp);

        if (hp <= 0)
        {
            gameObject.SetActive(false);
            return true;
        }
        return false;
    }
}
