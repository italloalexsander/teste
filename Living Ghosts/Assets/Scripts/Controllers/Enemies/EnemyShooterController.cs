﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyShooterController : EnemyController
{
    public float moveSpeed = 24f;
    public float jumpHeight = 8f;
    public float timeToJumpApex = 0.4f;
    public float accelerationTimeAir = 0.2f;
    public float accelerationTimeGround = 0.1f;
    public float maxViewDistance = 30f;
    public float maxShootDistance = 10f;

    public float fireRate = 1;
    public GameObject bullet;
    public Transform muzzle;

    private float lastShot;

    protected float gravity;
    protected float jumpVelocity;
    protected CharacterController2D characterController;
    protected Vector2 velocity;
    protected float velocityXSmoothing;
    protected Animator animator;

    // Use this for initialization
    override protected void Start()
    {
        base.Start();
        characterController = GetComponent<CharacterController2D>();
        characterController.onTriggerEnterEvent += OnTriggerEnterEvent;


        animator = GetComponent<Animator>();

        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        lastShot = Time.time;
    }

    void Update()
    {
        Move();
    }

    void FixedUpdate()
    {
        velocity = characterController.velocity;

        if (characterController.isGrounded)
        {
            velocity.y = 0;
        }

        var players = FindObjectsOfType<PlayerController>();

        foreach (var player in players)
        {
            if (((PlayerController)player).isActiveCharacter)
            {
                float distanceX = player.transform.position.x - transform.position.x;

                if (((PlayerController)player).isGhostCharacter)
                {
                    if (Mathf.Abs(distanceX) < maxViewDistance && distanceX > 0f)
                    {
                        GoRight();
                    }
                    else if (Mathf.Abs(distanceX) < maxViewDistance && distanceX < 0f)
                    {
                        GoLeft();
                    }
                    else
                    {
                        velocity.x = 0;
                    }
                }
                else
                {

                    if (Mathf.Abs(distanceX) < maxShootDistance)
                    {
                        if (((Time.time - lastShot) >= 1 / fireRate))
                        {
                            Shoot(player);
                        }
                        velocity.x = 0;
                    }
                    else if (Mathf.Abs(distanceX) < maxViewDistance && distanceX > 0f)
                    {
                        GoRight();
                    }
                    else if (Mathf.Abs(distanceX) < maxViewDistance && distanceX < 0f)
                    {
                        GoLeft();
                    }
                    else
                    {
                        velocity.x = 0;
                    }
                }
            }
        }
    }

    virtual protected void Move()
    {
        velocity.y += gravity * Time.deltaTime;
        characterController.move(velocity * Time.deltaTime);
    }

    protected void GoRight()
    {
        velocity.x = Mathf.SmoothDamp(velocity.x, moveSpeed * 0.4f, ref velocityXSmoothing, characterController.isGrounded ? accelerationTimeGround : accelerationTimeAir);

        if (transform.localScale.x < 0f)
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    protected void GoLeft()
    {
        velocity.x = Mathf.SmoothDamp(velocity.x, moveSpeed * -0.4f, ref velocityXSmoothing, characterController.isGrounded ? accelerationTimeGround : accelerationTimeAir);

        if (transform.localScale.x > 0f)
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    void Shoot(PlayerController player)
    {
        animator.SetTrigger("Shoot");

        lastShot = Time.time;
        GameObject bulletInstance = Instantiate(bullet, muzzle.position, muzzle.rotation);
        bulletInstance.GetComponent<Renderer>().sortingLayerName = "Bullet";
        bulletInstance.GetComponent<BulletController>().character = gameObject;
    }

    void OnTriggerEnterEvent(Collider2D obj)
    {
        if (obj.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (obj.gameObject.GetComponent<PlayerController>().isGhostCharacter)
            {
                Destroy(obj.gameObject);
                Time.timeScale = 0;
                SceneManager.LoadScene(2, LoadSceneMode.Additive);
            }
        }
    }
}
