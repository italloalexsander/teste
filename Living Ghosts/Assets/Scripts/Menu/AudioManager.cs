﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public AudioClip audioClip;
    AudioSource audioSource;
    private bool keepPlaying = true;

    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(SoundOut());
    }

    IEnumerator SoundOut()
    {
        while (keepPlaying)
        {
            audioSource.PlayOneShot(audioClip);
            yield return new WaitForSeconds(10);
        }
    }
}
