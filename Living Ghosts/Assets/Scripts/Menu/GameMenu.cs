﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	void FixedUpdate() {
		if (Input.GetButtonDown("Cancel"))
        {
            Pause();
        }
	}

    public void Pause ()
    {
        Time.timeScale = 0;
        SceneManager.LoadScene(1, LoadSceneMode.Additive);
    }

    public void Resume ()
    {
        SceneManager.UnloadSceneAsync(1);
        Time.timeScale = 1;
    }

    public void MainMenu ()
    {
        SceneManager.LoadScene(0);
    }

    public void ReturnToCheckpoint ()
    {
        SceneManager.UnloadSceneAsync(2);
        Time.timeScale = 1;

        print(PlayerController.checkPointCharacter != null);

        if (PlayerController.checkPointCharacter.transform.position != PlayerController.checkPointCharacter.GetComponent<PlayerController>().checkpoint.transform.position)
        {
            PlayerController.checkPointCharacter.GetComponent<PlayerController>().ResetToCheckpoint();
        }

        var vcam = GameObject.FindGameObjectWithTag("VirtualCamera").GetComponent<Cinemachine.CinemachineVirtualCamera>();
        vcam.Follow = PlayerController.checkPointCharacter.transform;

        PlayerController.checkPointCharacter.GetComponent<PlayerController>().isActiveCharacter = true;
        PlayerController.checkPointCharacter.GetComponent<BoxCollider2D>().isTrigger = false;
        PlayerController.checkPointCharacter.gameObject.layer = LayerMask.NameToLayer("Player"); 
        PlayerController.checkPointCharacter.GetComponent<SpriteRenderer>().sortingLayerName = "Player";

        var goArray = Resources.FindObjectsOfTypeAll<GameObject>();
        foreach (var obj in goArray)
        {
            if (obj.layer == LayerMask.NameToLayer("Playable") && obj.GetComponent<PlayerController>())
                obj.GetComponent<PlayerController>().ResetPosition();
            else if (obj.layer == LayerMask.NameToLayer("Enemy") && obj.GetComponent<EnemyController>())
            {
                obj.GetComponent<EnemyController>().ResetPosition();
            }
        }
    }
}
